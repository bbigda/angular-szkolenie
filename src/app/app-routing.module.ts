import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {DetailPageComponent} from './detail-page/detail-page.component';
// import {DetailPageComponent} from './detail-page/detail-page.component';

const routes: Routes = [
  {path: 'details/:id', component: DetailPageComponent},
  {path: '**', component: HomePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
