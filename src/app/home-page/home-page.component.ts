import {Component, OnInit} from '@angular/core';
import {StarWarsService} from '../star-wars/star-wars.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  characters: Array<any> = [];

  constructor(private starWarsService: StarWarsService) {
  }

  ngOnInit() {
    this.refresh();

  }

  handleSearch(searchTerm) {
    this.refresh(searchTerm);
  }

  private refresh(search?:string) {
    this.starWarsService.getCharacters(search).then(characters => {
      console.dir(characters);
      this.characters = characters;
    });
  }

}
