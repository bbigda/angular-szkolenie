import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output()
  onSearch: EventEmitter<string>;

  search: string;

  constructor() {
    this.onSearch = new EventEmitter();
  }

  ngOnInit() {
  }

  doSearch() {
    console.dir(this.search);
    this.onSearch.emit(this.search);
  }
}
