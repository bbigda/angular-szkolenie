import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StarWarsService {

  constructor(private http: HttpClient) {
  }

  public test() {
    return 'STAR WARS';
  }

  public getCharacters(search?: string): Promise<any> {

    let params = new HttpParams();

    if (search) {
      params = params.set('search', search);
    }
    return this.http.get('https://swapi.co/api/people',
      {params: params})
      .toPromise()
      .then(response => response['results']);
  }
}
